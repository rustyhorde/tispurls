//! tispurls - Lispish REPL in Rust
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy, clippy_pedantic))]
#![deny(missing_docs)]
#[macro_use]
extern crate clap;
#[macro_use]
extern crate lazy_static;
extern crate linenoise;
extern crate regex;
extern crate repl;

mod core;
mod env;
mod evaler;
mod reader;
mod types;
mod utils;

use clap::{App, AppSettings, Arg};
use env::TpReplEnv;
use reader::read_str;
use repl::{Repl, ReplEnv};
use std::default::Default;
use std::io::{self, Write};
use std::process;
use types::{strn_type, TpError, TpResult, TpVal};
use types::TpType::Sym;

const PREAMBLE: &'static str = "Welcome to tispurls, start lisping!!!";

struct TpRepl;

impl TpRepl {
    fn re(&self, input: &str, env: &TpReplEnv) -> TpResult {
        self.read(input.to_owned(), env).and_then(|r| self.eval(r, env))
    }

    fn rep(&self, input: &str, env: &TpReplEnv) -> TpResult {
        self.read(input.to_owned(), env)
            .and_then(|r| self.eval(r, env))
            .and_then(|r| self.print(r, env))
    }
}

impl Repl<TpVal, TpError, TpReplEnv> for TpRepl {
    fn preamble(&self, _: &TpReplEnv) -> &TpRepl {
        io::stdout().write_fmt(format_args!("{}\n", PREAMBLE)).unwrap_or(());
        io::stdout().flush().unwrap_or(());
        self
    }

    fn read(&self, input: String, _: &TpReplEnv) -> TpResult {
        match read_str(&input) {
            Ok(r) => {
                linenoise::history_add(&input);
                Ok(r)
            }
            Err(e) => Err(e),
        }
    }

    fn eval(&self, ast: TpVal, env: &TpReplEnv) -> TpResult {
        evaler::eval(ast, env.top().clone())
    }

    fn print(&self, cmd: TpVal, _: &TpReplEnv) -> TpResult {
        io::stdout().write_fmt(format_args!("{}\n", cmd.pr_str(true))).unwrap_or(());
        io::stdout().flush().unwrap_or(());
        Ok(cmd)
    }

    fn _loop(&self, env: &TpReplEnv) {
        if env.preamble() {
            self.preamble(env);
        }

        let green = "\x1b[32;1m";
        let reset = "\x1b[0m";

        let mut p = String::new();
        let prompt = env.prompt();

        if env.colorize() {
            p.push_str(green);
            p.push_str(prompt);
            p.push_str(reset);
        } else {
            p.push_str(prompt);
        }

        loop {
            match linenoise::input(&p) {
                None => break,
                Some(line) => {
                    match self.rep(&line, env) {
                        Err(e) => io::stderr().write_fmt(format_args!("{}\n", e)).unwrap_or(()),
                        Ok(ref o) if self.break_loop(o, env) => break,
                        Ok(_) => {}
                    }
                }
            }
        }
    }

    fn break_loop(&self, cmd: &TpVal, _: &TpReplEnv) -> bool {
        match **cmd {
            Sym(ref s) => {
                match &s[..] {
                    "exit" => true,
                    _ => false,
                }
            }
            _ => false,
        }
    }
}

// rustc 1.10.0-nightly (dd6e8d45e 2016-05-23)
// binary: rustc
// commit-hash: dd6e8d45e183861d44ed91a99f0a50403b2776a3
// commit-date: 2016-05-23
// host: x86_64-unknown-linux-gnu
// release: 1.10.0-nightly
#[cfg_attr(feature="clippy", allow(print_stdout, use_debug))]
fn main() {
    let mut env: TpReplEnv = Default::default();
    let matches = App::new("tispurls")
        .setting(AppSettings::TrailingVarArg)
        .author("Jason G. Ozias <jason.g.ozias@gmail.com>")
        .version(crate_version!())
        .about("Lispish REPL in Rust")
        .arg(Arg::with_name("colorize").short("c").long("colorize").help("Colorize the output"))
        .arg(Arg::with_name("prompt")
            .short("p")
            .long("prompt")
            .help("Set the prompt for the REPL")
            .takes_value(true))
        .arg(Arg::with_name("file")
            .short("f")
            .long("file")
            .help("Process the given mal file and exit")
            .takes_value(true))
        .arg(Arg::with_name("rest").multiple(true))
        .get_matches();

    if matches.is_present("colorize") {
        env.set_colorize(true);
    }

    if let Some(prompt) = matches.value_of("prompt") {
        env.set_prompt_str(prompt.to_owned());
    }

    env.set_histfile(None);

    let repl = TpRepl;

    // core.mal: defined using the language itself
    repl.re("(def! *host_language* \"rust\")", &env).expect("Unable to define *host_language*");
    repl.re("(def! not (fn* [a] (if a false true)))", &env).expect("Unable to define not");
    repl.re("(def! sq (fn* [a] (* a a)))", &env).expect("Unable to define sq");
    repl.re("(def! load-file (fn* (f) (eval (read-string (str \"(do \" (slurp f) \")\")))))",
            &env)
        .expect("Unable to define load-file");
    repl.re("(defmacro! cond (fn* (& xs) (if (> (count xs) 0) (list 'if (first xs) (if (> (count \
             xs) 1) (nth xs 1) (throw \"odd number of forms to cond\")) (cons 'cond (rest (rest \
             xs)))))))",
            &env)
        .expect("Unable to define cond");
    repl.re("(defmacro! or (fn* (& xs) (if (empty? xs) nil (if (= 1 (count xs)) (first xs) \
             `(let* (or_FIXME ~(first xs)) (if or_FIXME or_FIXME (or ~@(rest xs))))))))",
            &env)
        .expect("Unable to define or");

    if let Some(file) = matches.value_of("file") {
        if let Some(rest) = matches.values_of("rest") {
            let argv = rest.map(|a| strn_type(a.to_owned())).collect::<Vec<TpVal>>();
            env.set_argv(argv);
        }

        let lf = format!("(load-file \"{}\")", file);
        return match repl.rep(&lf, &env) {
            Ok(_) => process::exit(0),
            Err(str) => {
                println!("Error: {:?}", str);
                process::exit(1);
            }
        };
    } else {
        repl._loop(&env);
    }
}
