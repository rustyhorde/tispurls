use env::{Env, env_bind, env_new};
use self::TpError::{Arity, Syntax, Underflow, Val};
use self::TpType::{Atom, Defun, False, Float, Func, Hashmap, Int, List, Nil, Strn, Sym, True,
                   Vector};
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::num::{ParseFloatError, ParseIntError};
use std::rc::Rc;
use std::cell::RefCell;
use utils::{escape_str, pr_list};

pub enum TpType {
    Defun(TpFuncData),
    False,
    Float(f64),
    Func(fn(Vec<TpVal>) -> TpResult),
    Hashmap(HashMap<String, TpVal>),
    Int(isize),
    List(Vec<TpVal>),
    Nil,
    Strn(String),
    Sym(String),
    True,
    Vector(Vec<TpVal>),
    Atom(RefCell<TpVal>),
}

impl fmt::Debug for TpType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.pr_str(true))
    }
}

impl fmt::Display for TpType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.pr_str(true))
    }
}

impl PartialEq for TpType {
    fn eq(&self, other: &TpType) -> bool {
        match (self, other) {
            (&Nil, &Nil) | (&True, &True) | (&False, &False) => true,
            (&Float(ref a), &Float(ref b)) => a == b,
            (&Int(ref a), &Int(ref b)) => a == b,
            (&Strn(ref a), &Strn(ref b)) => a == b,
            (&Sym(ref a), &Sym(ref b)) => a == b,
            (&List(ref a), &List(ref b)) |
            (&Vector(ref a), &Vector(ref b)) |
            (&List(ref a), &Vector(ref b)) |
            (&Vector(ref a), &List(ref b)) => a == b,
            (&Hashmap(ref a), &Hashmap(ref b)) => a == b,
            // TODO: fix this
            // (&Func(_), &Func(_)) |
            // (&Defun(_), &Defun(_)) => false,
            _ => false,
        }
    }
}

impl TpType {
    pub fn pr_str(&self, print_readably: bool) -> String {
        match *self {
            Atom(ref v) => format!("(atom {})", &**v.borrow()),
            Defun(ref data) => format!("(fn* {} {})", data.params, data.exp),
            False => "false".to_owned(),
            Float(f) => f.to_string(),
            Func(_) => "#<function ...>".to_owned(),
            Hashmap(ref v) => {
                let mut base = String::from("{");
                for (i, (k, v)) in v.iter().enumerate() {
                    if i != 0 {
                        base.push(' ');
                    }
                    if k.starts_with("\u{29e}") {
                        let (_, suffix) = k.split_at(2);
                        base.push(':');
                        base.push_str(suffix);
                    } else if print_readably {
                        base.push_str(&escape_str(k));
                    } else {
                        base.push_str(k);
                    }
                    base.push(' ');
                    base.push_str(&v.pr_str(print_readably));
                }
                let mut out = String::from(base.trim_right());
                out.push('}');
                out
            }
            Int(i) => i.to_string(),
            List(ref v) => pr_list(v, print_readably, "(", ")", " "),
            Nil => "nil".to_owned(),
            Strn(ref s) => {
                if s.starts_with("\u{29e}") {
                    let (_, suffix) = s.split_at(2);
                    format!(":{}", suffix)
                } else if print_readably {
                    escape_str(s)
                } else {
                    s.clone()
                }
            }
            Sym(ref s) => {
                match &s[..] {
                    "exit" => "exiting...".to_owned(),
                    _ => s.clone(),
                }
            }
            True => "true".to_owned(),
            Vector(ref v) => pr_list(v, print_readably, "[", "]", " "),
        }
    }

    pub fn apply(&self, args: Vec<TpVal>) -> TpResult {
        match *self {
            Func(f) => f(args),
            Defun(ref data) => {
                let mfc = data.clone();
                let alst = list_type(args);
                let new_env = env_new(Some(mfc.env.clone()));
                match env_bind(&new_env, mfc.params, alst) {
                    Ok(_) => (mfc.eval)(mfc.exp, new_env),
                    Err(e) => Err(Syntax(e)),
                }
            }
            _ => Err(Syntax("attempt to call non-function".to_owned())),
        }
    }
}

pub type TpVal = Rc<TpType>;

#[derive(Clone)]
pub struct TpFuncData {
    pub eval: fn(TpVal, Env) -> TpResult,
    pub exp: TpVal,
    pub env: Env,
    pub params: TpVal,
    pub is_macro: bool,
}

impl fmt::Display for TpFuncData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("TpFuncData")
            .field("params", &self.params)
            .field("exp", &self.exp)
            .finish()
    }
}

#[derive(Debug)]
pub enum TpError {
    Arity(String),
    Env(String),
    Syntax(String),
    Underflow,
    Val(TpVal),
}

impl fmt::Display for TpError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Arity(ref s) => write!(f, "Arity Error: {}", s),
            TpError::Env(ref s) => write!(f, "Env Error: {}", s),
            Syntax(ref s) => write!(f, "Syntax Error: {}", s),
            Underflow => write!(f, "Token Underflow"),
            Val(ref v) => write!(f, "Error: {}", v),
        }
    }
}

impl Error for TpError {
    fn description(&self) -> &str {
        match *self {
            Arity(ref s) |
            TpError::Env(ref s) |
            Syntax(ref s) => s,
            Underflow => "Not enough tokens were found!",
            Val(_) => "VAL",
        }
    }
}

impl From<ParseIntError> for TpError {
    fn from(e: ParseIntError) -> TpError {
        Syntax(e.description().to_owned())
    }
}

impl From<ParseFloatError> for TpError {
    fn from(e: ParseFloatError) -> TpError {
        Syntax(e.description().to_owned())
    }
}

pub type TpResult = Result<TpVal, TpError>;

// Scalars
pub fn nil_type() -> TpVal {
    Rc::new(Nil)
}

pub fn true_type() -> TpVal {
    Rc::new(True)
}

pub fn false_type() -> TpVal {
    Rc::new(False)
}

pub fn int_type(i: isize) -> TpVal {
    Rc::new(Int(i))
}

pub fn float_type(f: f64) -> TpVal {
    Rc::new(Float(f))
}

// Strings
pub fn strn_type(s: String) -> TpVal {
    Rc::new(Strn(s))
}

// Symbols
pub fn sym_type(sym: String) -> TpVal {
    Rc::new(Sym(sym))
}

// Lists
pub fn list_type(seq: Vec<TpVal>) -> TpVal {
    Rc::new(List(seq))
}

// Vectors
pub fn vector_type(seq: Vec<TpVal>) -> TpVal {
    Rc::new(Vector(seq))
}

// Hash Maps
pub fn hash_map_type(seq: Vec<TpVal>) -> TpResult {
    if seq.len() % 2 == 1 {
        return Err(Syntax("Odd number of hashmap keys/values".to_owned()));
    }

    let mut hm = HashMap::new();

    for pair in seq.chunks(2) {
        let k = match pair.get(0) {
            Some(mv) => {
                match *mv.clone() {
                    Strn(ref s) => s.to_string(),
                    _ => return Err(Syntax("key is not a string in hashmap call".to_owned())),
                }
            }
            None => break,
        };

        let v = match pair.get(1) {
            Some(v) => v.clone(),
            None => return Err(Syntax("value is not valid in hashmap call".to_owned())),
        };

        hm.insert(k, v);
    }

    Ok(Rc::new(Hashmap(hm)))
}

// Functions
pub fn func_type(f: fn(Vec<TpVal>) -> TpResult) -> TpVal {
    Rc::new(Func(f))
}

pub fn defun_type(eval: fn(TpVal, Env) -> TpResult, exp: TpVal, env: Env, params: TpVal) -> TpVal {
    Rc::new(Defun(TpFuncData {
        eval: eval,
        exp: exp,
        env: env,
        params: params,
        is_macro: false,
    }))
}

pub fn defun_d_type(mfd: TpFuncData) -> TpVal {
    Rc::new(Defun(mfd))
}

// Atoms
pub fn atom_type(val: TpVal) -> TpVal {
    Rc::new(Atom(RefCell::new(val)))
}
