use core::ns;
use repl::ReplEnv;
use std::collections::HashMap;
use std::path::PathBuf;
use std::rc::Rc;
use std::cell::RefCell;
use types::{list_type, nil_type, sym_type, TpResult, TpVal};
use types::TpType::{List, Sym, Vector};
use types::TpError;

#[derive(Debug)]
pub struct TpReplEnv {
    colorize: bool,
    histfile: Option<PathBuf>,
    prompt_str: String,
    top: Env,
}

#[derive(Debug)]
pub struct EvalEnv {
    outer: Option<Env>,
    data: HashMap<String, TpVal>,
}

pub type Env = Rc<RefCell<EvalEnv>>;

pub fn env_new(outer: Option<Env>) -> Env {
    let mut data = HashMap::new();
    data.insert("exit".to_owned(), sym_type("exit".to_owned()));

    Rc::new(RefCell::new(EvalEnv {
        outer: outer,
        data: data,
    }))
}

pub fn env_set(env: &Env, key: TpVal, val: TpVal) {
    if let Sym(ref k) = *key {
        env.borrow_mut().data.insert(k.to_string(), val);
    }
}

pub fn env_get(env: &Env, key: &TpVal) -> TpResult {
    match **key {
        Sym(ref k) => {
            match env_find(env, key) {
                Some(e) => {
                    match e.borrow().data.get(k) {
                        Some(v) => Ok(v.clone()),
                        None => Ok(nil_type()),
                    }
                }
                None => Err(TpError::Env(format!("'{}' not found", k))),
            }
        }
        _ => Err(TpError::Env("env_get called with non-symbol key".to_string())),
    }
}

pub fn env_find(env: &Env, key: &TpVal) -> Option<Env> {
    match **key {
        Sym(ref k) => {
            let map = env.borrow();
            if map.data.contains_key(k) {
                Some(env.clone())
            } else {
                match map.outer {
                    Some(ref e) => env_find(e, key),
                    None => None,
                }
            }
        }
        _ => None,
    }
}

#[cfg_attr(feature="clippy", allow(mut_mut))]
pub fn env_bind(env: &Env, binds: TpVal, exprs: TpVal) -> Result<Env, String> {
    let mut variadic = false;
    match *binds {
        List(ref bind_args) |
        Vector(ref bind_args) => {
            match *exprs {
                List(ref exprs_args) |
                Vector(ref exprs_args) => {
                    let mut it = bind_args.iter().enumerate();
                    for (i, b) in it.by_ref() {
                        match **b {
                            Sym(ref strn) => {
                                if *strn == "&" {
                                    variadic = true;
                                    break;
                                } else {
                                    if let Some(args) = exprs_args.get(i) {
                                        env_set(env, b.clone(), args.clone())
                                    } else {
                                        return Err("Unable to bind exprs".to_owned());
                                    }
                                }
                            }
                            _ => return Err("non-symbol bind".to_string()),
                        }
                    }
                    if variadic {
                        let (i, sym) = it.next().expect("Unable to evaluate variadic");
                        match **sym {
                            Sym(_) => {
                                let (_, rest) = exprs_args.split_at(i - 1);
                                env_set(env, sym.clone(), list_type(rest.to_vec()));
                            }
                            _ => return Err("& bind to non-symbol".to_string()),
                        }
                    }
                    Ok(env.clone())
                }
                _ => Err("exprs must be a list".to_string()),
            }
        }
        _ => Err("binds must be a list".to_string()),
    }
}

pub fn env_root(env: &Env) -> Env {
    match env.borrow().outer {
        Some(ref ei) => env_root(ei),
        None => env.clone(),
    }
}

impl Default for TpReplEnv {
    fn default() -> TpReplEnv {
        let eval_env = env_new(None);
        env_set(&eval_env, sym_type("*ARGV*".to_owned()), list_type(vec![]));

        for (k, v) in ns().into_iter() {
            env_set(&eval_env, sym_type(k), v);
        }

        TpReplEnv {
            colorize: false,
            histfile: None,
            prompt_str: "tp > ".to_owned(),
            top: eval_env,
        }
    }
}

impl TpReplEnv {
    pub fn top(&self) -> &Env {
        &self.top
    }

    pub fn set_argv(&self, argv: Vec<TpVal>) -> &TpReplEnv {
        let eval_env = &self.top;
        env_set(eval_env, sym_type("*ARGV*".to_owned()), list_type(argv));
        self
    }

    pub fn set_colorize(&mut self, colorize: bool) -> &mut TpReplEnv {
        self.colorize = colorize;
        self
    }

    pub fn set_prompt_str(&mut self, prompt: String) -> &mut TpReplEnv {
        self.prompt_str = prompt;
        self
    }

    pub fn set_histfile(&mut self, path: Option<PathBuf>) -> &mut TpReplEnv {
        self.histfile = path;
        self
    }
}

impl ReplEnv for TpReplEnv {
    fn preamble(&self) -> bool {
        true
    }

    fn colorize(&self) -> bool {
        self.colorize
    }

    fn prompt(&self) -> &str {
        &self.prompt_str
    }
}
