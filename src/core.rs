use linenoise;
use reader::read_str;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, Read, Write};
use types::{atom_type, false_type, float_type, func_type, hash_map_type, int_type, list_type,
            nil_type, strn_type, sym_type, TpResult, TpVal, true_type, vector_type};
use types::TpType::{Atom, False, Float, Hashmap, Int, List, Nil, Strn, Sym, True, Vector};
use types::TpError::{Arity, Syntax, Val};
use utils;

// Errors/Exceptions
fn throw(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to throw call".to_owned()));
    }
    if let Some(a0) = seq.get(0) {
        Err(Val(a0.clone()))
    } else {
        Err(Syntax("Unable to get first throw arg".to_owned()))
    }
}

// Comparison Functions
fn is_equal(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("Wrong arity to equal? call".to_owned()));
    }
    if let Some(zero) = seq.get(0) {
        if let Some(one) = seq.get(1) {
            if zero == one {
                Ok(true_type())
            } else {
                Ok(false_type())
            }
        } else {
            Err(Syntax("Unable to get second value from seq".to_owned()))
        }
    } else {
        Err(Syntax("Unable to get first value from seq".to_owned()))
    }
}

fn is_nil(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to nil? call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            Nil => Ok(true_type()),
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Invalid nil? call".to_owned()))
    }
}

fn is_true(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to true? call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            True => Ok(true_type()),
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Invalid true? call".to_owned()))
    }
}

fn is_false(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to false? call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            False => Ok(true_type()),
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Invalid false? call".to_owned()))
    }
}

// Core Math Functions
fn bool_int_op<F>(f: F, seq: Vec<TpVal>) -> TpResult
    where F: FnOnce(isize, isize) -> bool
{
    if seq.len() > 2 {
        return Err(Arity("Supplied more than 2 arguments".to_owned()));
    }
    if let Some(a) = seq.get(0) {
        match **a {
            Int(a0) => {
                if let Some(b) = seq.get(1) {
                    match **b {
                        Int(a1) => {
                            if f(a0, a1) {
                                Ok(true_type())
                            } else {
                                Ok(false_type())
                            }
                        }
                        _ => Err(Syntax("Second argument must be an int".to_owned())),
                    }
                } else {
                    Err(Syntax("Unable to access second argument".to_owned()))
                }
            }
            _ => Err(Syntax("First argument must be an int".to_owned())),
        }
    } else {
        Err(Syntax("Unable to access first argument".to_owned()))
    }
}

fn bool_float_op<F>(f: F, seq: Vec<TpVal>) -> TpResult
    where F: FnOnce(f64, f64) -> bool
{
    if seq.len() > 2 {
        return Err(Arity("Supplied more than 2 arguments".to_owned()));
    }
    if let (Some(a), Some(b)) = (seq.get(0), seq.get(1)) {
        match **a {
            Float(a0) => {
                match **b {
                    Float(b0) => {
                        if f(a0, b0) {
                            Ok(true_type())
                        } else {
                            Ok(false_type())
                        }
                    }
                    _ => Err(Syntax("Second argument must be a float".to_owned())),
                }
            }
            _ => Err(Syntax("First argument must be a float".to_owned())),
        }
    } else {
        Err(Syntax("Unable to access arguments".to_owned()))
    }
}

fn has_float(seq: &[TpVal]) -> bool {
    seq.iter().any(|num| match **num {
        Float(_) => true,
        _ => false,
    })
}

#[cfg_attr(feature="clippy", allow(cast_precision_loss))]
fn to_floats(seq: Vec<TpVal>) -> Vec<TpVal> {
    seq.iter()
        .map(|num| match **num {
            Int(i) => float_type(i as f64),
            Float(f) => float_type(f),
            _ => nil_type(),
        })
        .collect()
}

fn lt(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("wrong arity for <".to_owned()));
    }
    if has_float(&seq) {
        bool_float_op(|i, j| i < j, to_floats(seq))
    } else {
        bool_int_op(|i, j| i < j, seq)
    }
}

fn lte(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("wrong arity for <=".to_owned()));
    }
    if has_float(&seq) {
        bool_float_op(|i, j| i <= j, to_floats(seq))
    } else {
        bool_int_op(|i, j| i <= j, seq)
    }
}

fn gt(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("wrong arity for >".to_owned()));
    }
    if has_float(&seq) {
        bool_float_op(|i, j| i > j, to_floats(seq))
    } else {
        bool_int_op(|i, j| i > j, seq)
    }
}

fn gte(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("wrong arity for >=".to_owned()));
    }
    if has_float(&seq) {
        bool_float_op(|i, j| i >= j, to_floats(seq))
    } else {
        bool_int_op(|i, j| i >= j, seq)
    }
}

fn int_op<F>(f: F, a: Vec<TpVal>) -> TpResult
    where F: FnOnce(Vec<isize>) -> isize
{
    let mut ints: Vec<isize> = Vec::new();
    for val in a {
        if let Int(v) = *val {
            ints.push(v);
        } else {
            return Err(Syntax("all arguments must be ints".to_owned()));
        }
    }
    Ok(int_type(f(ints)))
}

#[cfg_attr(feature="clippy", allow(cast_precision_loss))]
fn float_op<F>(f: F, a: Vec<TpVal>) -> TpResult
    where F: FnOnce(Vec<f64>) -> f64
{
    let mut floats: Vec<f64> = Vec::new();
    for val in a {
        match *val {
            Float(v) => floats.push(v),
            Int(i) => floats.push(i as f64),
            _ => return Err(Syntax("invalid number argument".to_owned())),
        }
    }
    Ok(float_type(f(floats)))
}

fn add(seq: Vec<TpVal>) -> TpResult {
    if seq.is_empty() {
        return Err(Arity("wrong arity for +".to_owned()));
    }
    if has_float(&seq) {
        float_op(|v| v.iter().fold(0.0, |acc, &x| x + acc), seq)
    } else {
        int_op(|v| v.iter().fold(0, |acc, &x| x + acc), seq)
    }
}

fn sub(seq: Vec<TpVal>) -> TpResult {
    if seq.is_empty() {
        return Err(Arity("wrong arity for -".to_owned()));
    }
    if has_float(&seq) {
        float_op(|v| {
            let (f, rest) = v.split_at(1);
            let a0 = f.get(0).expect("Unable to get first argument");
            if rest.is_empty() {
                0.0 - a0
            } else {
                rest.iter().fold(*a0, |acc, &x| acc - x)
            }
        },
                 to_floats(seq))
    } else {
        int_op(|v| {
            let (f, rest) = v.split_at(1);
            let a0 = f.get(0).expect("Unable to get first argument");
            if rest.is_empty() {
                0 - a0
            } else {
                rest.iter().fold(*a0, |acc, &x| acc - x)
            }
        },
               seq)
    }
}

fn mul(seq: Vec<TpVal>) -> TpResult {
    if seq.is_empty() {
        return Err(Arity("wrong arity for *".to_owned()));
    }
    if has_float(&seq) {
        float_op(|v| v.iter().fold(1.0, |acc, &x| x * acc), seq)
    } else {
        int_op(|v| v.iter().fold(1, |acc, &x| x * acc), seq)
    }
}

fn div(seq: Vec<TpVal>) -> TpResult {
    if seq.is_empty() {
        return Err(Arity("wrong arity for /".to_owned()));
    }
    if has_float(&seq) {
        float_op(|v| {
            let (f, rest) = v.split_at(1);
            let a0 = f.get(0).expect("Unable to get first argument");
            if rest.is_empty() {
                *a0
            } else {
                rest.iter().fold(*a0, |acc, &x| acc / x)
            }
        },
                 to_floats(seq))
    } else {
        int_op(|v| {
            let (f, rest) = v.split_at(1);
            let a0 = f.get(0).expect("Unable to get first argument");
            if rest.is_empty() {
                *a0
            } else if *a0 == 0 {
                0
            } else {
                rest.iter().fold(*a0, |acc, &x| acc / x)
            }
        },
               seq)
    }
}

// Core String Functions
fn pr_str(a: Vec<TpVal>) -> TpResult {
    Ok(strn_type(utils::pr_list(&a, true, "", "", " ")))
}

fn str(a: Vec<TpVal>) -> TpResult {
    Ok(strn_type(utils::pr_list(&a, false, "", "", "")))
}

fn prn(a: Vec<TpVal>) -> TpResult {
    let list_out = utils::pr_list(&a, true, "", "", " ");
    write!(io::stdout(), "{}\n", list_out).expect("Unable to write to stdout");
    Ok(nil_type())
}

fn println(a: Vec<TpVal>) -> TpResult {
    let list_out = utils::pr_list(&a, false, "", "", " ");
    write!(io::stdout(), "{}\n", list_out).expect("Unable to write to stdout");
    Ok(nil_type())
}

fn read_string(a: Vec<TpVal>) -> TpResult {
    if let Some(a0) = a.get(0) {
        match **a0 {
            Strn(ref s) => read_str(s),
            _ => Err(Syntax("read_string called with non-string".to_owned())),
        }
    } else {
        Err(Syntax("Unable to get first argument".to_owned()))
    }
}

fn slurp(a: Vec<TpVal>) -> TpResult {
    if let Some(a0) = a.get(0) {
        match **a0 {
            Strn(ref s) => {
                let mut fs = String::new();
                match File::open(s).and_then(|mut f| f.read_to_string(&mut fs)) {
                    Ok(_) => Ok(strn_type(fs)),
                    Err(e) => Err(Syntax(e.to_string())),
                }
            }
            _ => Err(Syntax("slurp called with non-string".to_owned())),
        }
    } else {
        Err(Syntax("Unable to get first argument".to_owned()))
    }
}

fn readline(seq: Vec<TpVal>) -> TpResult {
    let a0 = seq.get(0).expect("Unable to get first argument");

    match **a0 {
        Strn(ref s) => match linenoise::input(&s) {
            Some(line) => Ok(strn_type(line)),
            None       => Ok(nil_type()),
        },
        _ => Err(Syntax("read_string called with non-string".to_owned()))
    }
}

// Core List Functions
fn list(seq: Vec<TpVal>) -> TpResult {
    Ok(list_type(seq))
}

fn is_list(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to list? call".to_owned()));
    }
    if let Some(arg) = seq.get(0) {
        match **arg {
            List(_) => Ok(true_type()),
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Unable to get first value from seq".to_owned()))
    }
}

// Core Sequence Functions
fn is_empty(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to empty? call".to_owned()));
    }
    if let Some(arg) = seq.get(0) {
        match **arg {
            List(ref v) | Vector(ref v) => {
                match v.len() {
                    0 => Ok(true_type()),
                    _ => Ok(false_type()),
                }
            }
            _ => Err(Syntax("empty? called on non-sequence".to_owned())),
        }
    } else {
        Err(Syntax("Unable to get first value from seq".to_owned()))
    }
}

#[cfg_attr(feature="clippy", allow(cast_possible_wrap))]
fn count(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to count call".to_owned()));
    }
    if let Some(arg) = seq.get(0) {
        match **arg {
            List(ref v) | Vector(ref v) => Ok(int_type(v.len() as isize)),
            Nil => Ok(int_type(0)),
            _ => Err(Syntax("empty? called on non-sequence".to_owned())),
        }
    } else {
        Err(Syntax("Unable to get first value from seq".to_owned()))
    }
}

fn cons(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("Wrong arity to cons call".to_owned()));
    }
    if let Some(a0) = seq.get(0) {
        if let Some(a1) = seq.get(1) {
            match **a1 {
                List(ref v) | Vector(ref v) => {
                    let mut new_v = v.clone();
                    new_v.insert(0, a0.clone());
                    Ok(list_type(new_v))
                }
                Nil => {
                    let mut v = Vec::new();
                    v.push(a0.clone());
                    Ok(list_type(v))
                }
                _ => Err(Syntax("Second arg to cons is not a sequence".to_owned())),
            }
        } else {
            Err(Syntax("Unable to get the second cons arg".to_owned()))
        }
    } else {
        Err(Syntax("Unable to get the first cons arg".to_owned()))
    }
}

fn concat(seq: Vec<TpVal>) -> TpResult {
    let mut new_v: Vec<TpVal> = vec![];
    for lst in &seq {
        match **lst {
            List(ref l) | Vector(ref l) => new_v.extend(l.clone()),
            _ => return Err(Syntax("concat called with non-sequence".to_owned())),
        }
    }
    Ok(list_type(new_v))
}

#[cfg_attr(feature="clippy", allow(cast_sign_loss))]
fn nth(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("Wrong arity to nth call".to_owned()));
    }
    if let (Some(a0), Some(a1)) = (seq.get(0), seq.get(1)) {
        let arg_seq = match **a0 {
            List(ref v) | Vector(ref v) => v,
            _ => return Err(Syntax("nth called with non-sequence".to_owned())),
        };
        let idx = match **a1 {
            Int(i) => i,
            _ => return Err(Syntax("nth called with non-integer index".to_owned())),
        } as usize;
        if idx >= arg_seq.len() {
            Err(Syntax("nth: index out of range".to_owned()))
        } else {
            if let Some(val) = arg_seq.get(idx) {
                Ok(val.clone())
            } else {
                Err(Syntax("Unable to get val at index".to_owned()))
            }
        }
    } else {
        Err(Syntax("Unable to get nth args".to_owned()))
    }
}

fn first(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to first call".to_owned()));
    }
    if let Some(a0) = seq.get(0) {
        let arg_seq = match **a0 {
            List(ref v) | Vector(ref v) => v,
            Nil => return Ok(nil_type()),
            _ => return Err(Syntax("first called with non-sequence".to_owned())),
        };
        if arg_seq.is_empty() {
            Ok(nil_type())
        } else {
            if let Some((first, _)) = arg_seq.split_first() {
                Ok(first.clone())
            } else {
                Err(Syntax("Unable to get first value".to_owned()))
            }
        }
    } else {
        Err(Syntax("Unable to get first arg".to_owned()))
    }
}

fn rest(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to rest call".to_owned()));
    }
    if let Some(a0) = seq.get(0) {
        let arg_seq = match **a0 {
            List(ref v) | Vector(ref v) => v,
            Nil => return Ok(list_type(vec![])),
            _ => return Err(Syntax("rest called with non-sequence".to_owned())),
        };
        if arg_seq.is_empty() {
            Ok(list_type(vec![]))
        } else {
            let (_, rest) = arg_seq.split_at(1);
            Ok(list_type(rest.to_vec()))
        }
    } else {
        Err(Syntax("Unable to get rest arg".to_owned()))
    }
}

fn apply(seq: Vec<TpVal>) -> TpResult {
    if seq.len() < 2 {
        return Err(Arity("apply call needs 2 or more arguments".to_owned()));
    }
    let (func, args) = seq.split_at(1);
    if let (Some(f), Some((last, rest))) = (func.get(0), args.split_last()) {
        let mut rest_vec = rest.to_vec();
        match **last {
            List(ref v) | Vector(ref v) => {
                rest_vec.extend(v.clone());
                (*f).apply(rest_vec)
            }
            _ => Err(Syntax("apply call with non-sequence".to_owned())),
        }
    } else {
        Err(Syntax("Invalid apply call".to_owned()))
    }
}

fn map(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("Wrong arity to map call".to_owned()));
    }
    let mut results: Vec<TpVal> = vec![];
    if let (Some(a0), Some(a1)) = (seq.get(0), seq.get(1)) {
        match **a1 {
            List(ref v) | Vector(ref v) => {
                for mv in v.iter() {
                    let res = try!((*a0).apply(vec![mv.clone()]));
                    results.push(res);
                }
            }
            _ => return Err(Syntax("map call with non-sequence".to_owned())),
        }
        Ok(list_type(results))
    } else {
        Err(Syntax("Invalid map call".to_owned()))
    }
}

fn is_seq(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("wrong arity to seq? call".to_owned()));
    }

    let a0 = seq.get(0).expect("unable to get first argument");

    match **a0 {
        List(_) | Vector(_) => Ok(true_type()),
        _ => Ok(false_type()),
    }
}

// Core Symbol Functions
fn symbol(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to symbol call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            Strn(ref s) => Ok(sym_type(s.to_string())),
            _ => return Err(Syntax("symbol called on non-string".to_owned())),
        }
    } else {
        Err(Syntax("Invalid symbol call".to_owned()))
    }
}

fn is_symbol(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to symbol? call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            Sym(_) => Ok(true_type()),
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Invalid symbol? call".to_owned()))
    }
}

// Core Keyword Functions
fn keyword(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to keyword call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            Strn(ref s) => {
                if s.starts_with("\u{29e}") {
                    Ok(strn_type(s.clone()))
                } else {
                    Ok(strn_type(format!("\u{29e}{}", s)))
                }
            }
            _ => return Err(Syntax("keyword called on non-string".to_owned())),
        }
    } else {
        Err(Syntax("Invalid keyword call".to_owned()))
    }
}

fn is_keyword(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to keyword? call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            Strn(ref s) => {
                if s.starts_with("\u{29e}") {
                    Ok(true_type())
                } else {
                    Ok(false_type())
                }
            }
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Invalid keyword? call".to_owned()))
    }
}

// Core Vector Functions
fn vector(seq: Vec<TpVal>) -> TpResult {
    Ok(vector_type(seq))
}

fn is_vector(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to vector? call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            Vector(_) => Ok(true_type()),
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Invalid vector? call".to_owned()))
    }
}

// Core Map Functions
fn hash_map(seq: Vec<TpVal>) -> TpResult {
    hash_map_type(seq)
}

fn is_map(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to map? call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            Hashmap(_) => Ok(true_type()),
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Invalid map? call".to_owned()))
    }
}

fn has_key(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("wrong arity to contains? call".to_owned()));
    }

    let a0 = seq.get(0).expect("Unable to get hash-map arg");
    let a1 = seq.get(1).expect("Unable to get key arg");

    match **a0 {
        Hashmap(ref hm) => {
            match **a1 {
                Strn(ref s) => {
                    if hm.contains_key(s) {
                        Ok(true_type())
                    } else {
                        Ok(false_type())
                    }
                }
                _ => Err(Syntax("contains? called with non-key arg".to_owned())),
            }
        }
        _ => Err(Syntax("contains? called with non-hash-map arg".to_owned())),
    }
}

fn keys(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("wrong arity to keys call".to_owned()));
    }

    let a0 = seq.get(0).expect("Unable to get hash-map arg");

    match **a0 {
        Hashmap(ref hm) => {
            let mut key_vec = Vec::new();
            for key in hm.keys() {
                key_vec.push(strn_type(key.clone()));
            }
            Ok(list_type(key_vec))
        }
        _ => Err(Syntax("keys called with non-hash-map arg".to_owned())),
    }
}

fn vals(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("wrong arity to vals call".to_owned()));
    }

    let a0 = seq.get(0).expect("Unable to get hash-map arg");

    match **a0 {
        Hashmap(ref hm) => {
            let mut vals_vec = Vec::new();
            for v in hm.values() {
                vals_vec.push(v.clone());
            }
            Ok(list_type(vals_vec))
        }
        _ => Err(Syntax("vals called with non-hash-map arg".to_owned())),
    }
}

fn assoc(seq: Vec<TpVal>) -> TpResult {
    if seq.len() < 3 {
        return Err(Arity("wrong arrity to assoc call".to_owned()));
    }
    if seq.len() % 2 == 0 {
        return Err(Syntax("Odd number of hashmap keys/values".to_owned()));
    }

    let (first, rest) = seq.split_at(1);
    if let Some(a0) = first.get(0) {
        let base_hash_map = match **a0 {
            Hashmap(ref hm) => hm.clone(),
            Nil => HashMap::new(),
            _ => return Err(Syntax("assoc onto non-hash map".to_owned())),
        };

        for pair in rest.chunks(2) {
            if let Some(k) = pair.get(0) {
                match **k {
                    Strn(_) => {}
                    _ => return Err(Syntax("key is not string in assoc call".to_owned())),
                }
            } else {
                return Err(Syntax("key is not string in assoc call".to_owned()));
            }
        }

        let mut rest_vec = rest.to_vec();

        for (k, v) in base_hash_map {
            rest_vec.push(strn_type(k));
            rest_vec.push(v);
        }

        hash_map_type(rest_vec)
    } else {
        Err(Syntax("Invalid assoc call".to_owned()))
    }
}

fn dissoc(seq: Vec<TpVal>) -> TpResult {
    if seq.len() < 2 {
        return Err(Arity("wrong arrity to dissoc call".to_owned()));
    }

    if let Some((first, rest)) = seq.split_first() {
        match **first {
            Hashmap(ref hm) => {
                let mut new_hm = hm.clone();
                for key in rest {
                    match **key {
                        Strn(ref s) => {
                            new_hm.remove(s);
                        }
                        _ => return Err(Syntax("key is not string in dissoc call".to_owned())),
                    }
                }

                let mut new_vec = Vec::new();

                for (k, v) in new_hm {
                    new_vec.push(strn_type(k));
                    new_vec.push(v);
                }
                hash_map_type(new_vec)
            }
            Nil => Ok(nil_type()),
            _ => return Err(Syntax("dissoc onto non-hash map".to_owned())),
        }
    } else {
        Err(Syntax("Invalid dissoc call".to_owned()))
    }
}

fn get(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("wrong arrity to get call".to_owned()));
    }

    if let (Some(hm), Some(key)) = (seq.get(0), seq.get(1)) {
        match **hm {
            Hashmap(ref hm) => {
                match **key {
                    Strn(ref k) => {
                        match hm.get(k) {
                            Some(v) => Ok(v.clone()),
                            None => Ok(nil_type()),
                        }
                    }
                    _ => return Err(Syntax("invalid key for get call".to_owned())),
                }
            }
            Nil => Ok(nil_type()),
            _ => return Err(Syntax("get on non-map type".to_owned())),
        }
    } else {
        Err(Syntax("Invalid get call".to_owned()))
    }
}

// Core Atom Functions
fn atom(seq: Vec<TpVal>) -> TpResult {
    if seq.len() > 1 {
        return Err(Arity("Wrong arity for atom call".to_owned()));
    }
    if let Some(a0) = seq.get(0) {
        Ok(atom_type(a0.clone()))
    } else {
        Err(Syntax("Unable to form atom".to_owned()))
    }
}

fn is_atom(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to atom? call".to_owned()));
    }
    if let Some(a0) = seq.get(0) {
        match **a0 {
            Atom(_) => Ok(true_type()),
            _ => Ok(false_type()),
        }
    } else {
        Err(Syntax("Unable to get first argument".to_owned()))
    }
}

fn deref(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 1 {
        return Err(Arity("Wrong arity to deref call".to_owned()));
    }
    if let Some(a0) = seq.get(0) {
        match **a0 {
            Atom(ref val) => Ok(val.borrow().clone()),
            _ => Err(Syntax("deref called on non-atom".to_owned())),
        }
    } else {
        Err(Syntax("Unable to get first argument".to_owned()))
    }
}

fn reset_bang(seq: Vec<TpVal>) -> TpResult {
    if seq.len() != 2 {
        return Err(Arity("Wrong arity to reset! call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        match **a0 {
            Atom(ref val) => {
                let mut val_cell = val.borrow_mut();
                if let Some(a1) = seq.get(1) {
                    *val_cell = a1.clone();
                    Ok(a1.clone())
                } else {
                    Err(Syntax("Unable to get second argument".to_owned()))
                }
            }
            _ => Err(Syntax("reset! called on non-atom".to_owned())),
        }
    } else {
        Err(Syntax("Unable to get first argument".to_owned()))
    }
}

fn swap_bang(seq: Vec<TpVal>) -> TpResult {
    if seq.len() < 2 {
        return Err(Arity("Wrong arity to swap! call".to_owned()));
    }

    if let Some(a0) = seq.get(0) {
        if let Some(a1) = seq.get(1) {
            let f = a1.clone();
            match **a0 {
                Atom(ref val) => {
                    let mut val_cell = val.borrow_mut();
                    let (_, rest) = seq.split_at(2);
                    let mut args = rest.to_vec();
                    args.insert(0, val_cell.clone());
                    *val_cell = try!(f.apply(args));
                    Ok(val_cell.clone())
                }
                _ => Err(Syntax("swap! called on non-atom".to_owned())),
            }
        } else {
            Err(Syntax("Unable to get second argument".to_owned()))
        }
    } else {
        Err(Syntax("Unable to get first argument".to_owned()))
    }
}

pub fn ns() -> HashMap<String, TpVal> {
    let mut ns = HashMap::new();

    // Error Handling
    ns.insert("throw".to_owned(), func_type(throw));

    // Comparison Operators
    ns.insert("=".to_owned(), func_type(is_equal));
    ns.insert("nil?".to_owned(), func_type(is_nil));
    ns.insert("true?".to_owned(), func_type(is_true));
    ns.insert("false?".to_owned(), func_type(is_false));

    // Integer Operations
    ns.insert("+".to_owned(), func_type(add));
    ns.insert("-".to_owned(), func_type(sub));
    ns.insert("*".to_owned(), func_type(mul));
    ns.insert("/".to_owned(), func_type(div));
    ns.insert("<".to_owned(), func_type(lt));
    ns.insert("<=".to_owned(), func_type(lte));
    ns.insert(">".to_owned(), func_type(gt));
    ns.insert(">=".to_owned(), func_type(gte));

    // String Operations
    ns.insert("pr-str".to_owned(), func_type(pr_str));
    ns.insert("str".to_owned(), func_type(str));
    ns.insert("prn".to_owned(), func_type(prn));
    ns.insert("println".to_owned(), func_type(println));
    ns.insert("read-string".to_owned(), func_type(read_string));
    ns.insert("slurp".to_owned(), func_type(slurp));
    ns.insert("readline".to_owned(), func_type(readline));

    // List Operations
    ns.insert("list".to_owned(), func_type(list));
    ns.insert("list?".to_owned(), func_type(is_list));

    // Sequence Operations
    ns.insert("empty?".to_owned(), func_type(is_empty));
    ns.insert("count".to_owned(), func_type(count));
    ns.insert("cons".to_owned(), func_type(cons));
    ns.insert("concat".to_owned(), func_type(concat));
    ns.insert("nth".to_owned(), func_type(nth));
    ns.insert("first".to_owned(), func_type(first));
    ns.insert("rest".to_owned(), func_type(rest));
    ns.insert("apply".to_owned(), func_type(apply));
    ns.insert("map".to_owned(), func_type(map));
    ns.insert("seq?".to_owned(), func_type(is_seq));

    // Symbol Operations
    ns.insert("symbol".to_owned(), func_type(symbol));
    ns.insert("symbol?".to_owned(), func_type(is_symbol));

    // Keyword Operations
    ns.insert("keyword".to_owned(), func_type(keyword));
    ns.insert("keyword?".to_owned(), func_type(is_keyword));

    // Vector Operations
    ns.insert("vector".to_owned(), func_type(vector));
    ns.insert("vector?".to_owned(), func_type(is_vector));

    // Hashmap Operations
    ns.insert("hash-map".to_owned(), func_type(hash_map));
    ns.insert("map?".to_owned(), func_type(is_map));
    ns.insert("assoc".to_owned(), func_type(assoc));
    ns.insert("dissoc".to_owned(), func_type(dissoc));
    ns.insert("get".to_owned(), func_type(get));
    ns.insert("contains?".to_owned(), func_type(has_key));
    ns.insert("keys".to_owned(), func_type(keys));
    ns.insert("vals".to_owned(), func_type(vals));

    // Atom Operations
    ns.insert("atom".to_owned(), func_type(atom));
    ns.insert("atom?".to_owned(), func_type(is_atom));
    ns.insert("deref".to_owned(), func_type(deref));
    ns.insert("reset!".to_owned(), func_type(reset_bang));
    ns.insert("swap!".to_owned(), func_type(swap_bang));

    ns
}
