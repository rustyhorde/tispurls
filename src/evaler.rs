use env::{env_bind, env_find, env_get, env_new, env_root, env_set, Env};
use linenoise;
use reader::read_str;
use std::error::Error;
use std::io::{self, Write};
use types::{defun_type, defun_d_type, hash_map_type, list_type, nil_type, strn_type, sym_type,
            TpResult, TpVal, vector_type};
use types::TpError::{Arity, Syntax, Val};
use types::TpType::{Defun, False, Func, Hashmap, Int, List, Nil, Sym, Vector};

fn is_pair(x: &TpVal) -> bool {
    match **x {
        List(ref lst) | Vector(ref lst) => !lst.is_empty(),
        _ => false,
    }
}

fn check_unquote(a0: &TpVal) -> bool {
    match **a0 {
        Sym(ref s) if *s == "unquote" => true,
        _ => false,
    }
}

fn check_spliceunquote(a0: &TpVal) -> bool {
    match **a0 {
        Sym(ref s) if *s == "splice-unquote" => true,
        _ => false,
    }
}

fn quasiquote(ast: TpVal) -> TpVal {
    if is_pair(&ast) {
        match *ast.clone() {
            List(ref args) | Vector(ref args) => {
                if let Some(a0) = args.get(0) {
                    if check_unquote(a0) {
                        if let Some(a1) = args.get(1) {
                            return a1.clone();
                        } else {
                            return nil_type();
                        }
                    }

                    let (_, rest) = args.split_at(1);
                    let rest_list = list_type(rest.to_vec());

                    if is_pair(a0) {
                        match **a0 {
                            List(ref a0args) |
                            Vector(ref a0args) => {
                                if let Some(zero) = a0args.get(0) {
                                    if check_spliceunquote(zero) {
                                        if let Some(one) = a0args.get(1) {
                                            return list_type(vec![sym_type("concat".to_owned()),
                                                                  one.clone(),
                                                                  quasiquote(rest_list)]);
                                        }
                                    }
                                }
                            }
                            _ => (),
                        }
                    }

                    list_type(vec![sym_type("cons".to_owned()),
                                   quasiquote(a0.clone()),
                                   quasiquote(rest_list)])
                } else {
                    nil_type()
                }
            }
            _ => nil_type(), // should never reach
        }
    } else {
        list_type(vec![sym_type("quote".to_owned()), ast])
    }
}

fn is_macro_call(ast: TpVal, env: Env) -> bool {
    let list_type = match *ast {
        List(ref l) => {
            if let Some(zero) = l.get(0) {
                zero
            } else {
                return false;
            }
        }
        _ => return false,
    };
    match **list_type {
        Sym(_) => {}
        _ => return false,
    }
    if env_find(&env, list_type).is_none() {
        return false;
    }
    let f = match env_get(&env, list_type) {
        Ok(f) => f,
        _ => return false,
    };
    match *f {
        Defun(ref mfd) => mfd.is_macro,
        _ => false,
    }
}


fn macroexpand(mut ast: TpVal, env: Env) -> TpResult {
    while is_macro_call(ast.clone(), env.clone()) {
        let ast2 = ast.clone();
        let args = match *ast2 {
            List(ref args) => args,
            _ => break,
        };
        if let Some((a0, rest)) = args.split_first() {
            let mf = match **a0 {
                Sym(_) => try!(env_get(&env, &a0)),
                _ => break,
            };
            match *mf {
                Defun(_) => ast = try!(mf.apply(rest.to_vec())),
                _ => break,
            }
        } else {
            return Err(Syntax("Unable to split args".to_owned()));
        }

    }
    Ok(ast)
}

pub fn eval_ast(ast: TpVal, env: Env) -> TpResult {
    match *ast {
        Sym(_) => env_get(&env, &ast),
        List(ref a) | Vector(ref a) => {
            let mut ast_vec: Vec<TpVal> = Vec::new();
            for mv in a.iter() {
                let mv2 = mv.clone();
                ast_vec.push(try!(eval(mv2, env.clone())));
            }
            match *ast {
                List(_) => Ok(list_type(ast_vec)),
                Vector(_) => Ok(vector_type(ast_vec)),
                _ => Err(Syntax("Invalid ast type".to_owned())),
            }
        }
        Hashmap(ref hm) => {
            let mut vec: Vec<TpVal> = Vec::new();
            for (key, value) in hm.iter() {
                vec.push(strn_type(key.clone()));
                vec.push(try!(eval(value.clone(), env.clone())));
            }
            hash_map_type(vec)
        }
        _ => Ok(ast.clone()),
    }
}

#[cfg_attr(feature="clippy", allow(cast_possible_truncation))]
pub fn eval(mut ast: TpVal, mut env: Env) -> TpResult {
    'tco: loop {
        match *ast {
            List(_) => {}
            _ => return eval_ast(ast, env),
        }

        // apply list
        ast = try!(macroexpand(ast, env.clone()));
        match *ast {
            List(_) => (),  // continue
            _ => return eval_ast(ast, env),
        }

        let tmp = ast;
        let (args, special) = match *tmp {
            List(ref args) => {
                if args.is_empty() {
                    return Ok(tmp.clone());
                }
                if let Some(a0) = args.first() {
                    match **a0 {
                        Sym(ref a0sym) => (args, &a0sym[..]),
                        _ => (args, "__<fn*>__"),
                    }
                } else {
                    return Err(Syntax("Unable to evaluate first argument".to_owned()));
                }
            }
            _ => return Err(Syntax("Expected list".to_owned())),
        };

        match special {
            "def!" => {
                if let Some(a1) = (*args).get(1) {
                    if let Some(a2) = (*args).get(2) {
                        let r = try!(eval(a2.clone(), env.clone()));
                        match **a1 {
                            Sym(_) => {
                                env_set(&env.clone(), a1.clone(), r.clone());
                                return Ok(r);
                            }
                            _ => return Err(Syntax("def! of non-symbol".to_owned())),
                        }
                    } else {
                        return Err(Syntax("Unable to evaluate second def! arg".to_owned()));
                    }
                } else {
                    return Err(Syntax("Unable to evaluate first def! arg".to_owned()));
                }
            }
            "let*" => {
                let let_env = env_new(Some(env.clone()));
                if let Some(a1) = (*args).get(1) {
                    if let Some(a2) = (*args).get(2) {
                        match **a1 {
                            List(ref binds) |
                            Vector(ref binds) => {
                                let mut it = binds.iter();
                                while it.len() >= 2 {
                                    let b = it.next().expect("Unable to evaluate bind");
                                    let exp = it.next()
                                        .expect("Unable to evaluate bind expression");
                                    if let Sym(_) = **b {
                                        let r = try!(eval(exp.clone(), let_env.clone()));
                                        env_set(&let_env, b.clone(), r);
                                    } else {
                                        return Err(Syntax("let* with non-symbol binding"
                                            .to_owned()));
                                    }
                                }
                            }
                            _ => return Err(Syntax("let* with non-list bindings".to_owned())),
                        }
                        ast = a2.clone();
                        env = let_env.clone();
                        continue 'tco;
                    } else {
                        return Err(Syntax("Unable to evaluate second def! arg".to_owned()));
                    }
                } else {
                    return Err(Syntax("Unable to evaluate first def! arg".to_owned()));
                }
            }
            "do" => {
                if let Some((_, do_args)) = args.split_first() {
                    let mut v_args = do_args.to_vec();
                    if let Some(last) = v_args.pop() {
                        let el = list_type(v_args);
                        try!(eval_ast(el, env.clone()));
                        ast = last;
                        continue 'tco;
                    } else {
                        return Err(Syntax("Invalid do call".to_owned()));
                    }
                } else {
                    return Err(Syntax("Invalid do call".to_owned()));
                }
            }
            "if" => {
                if let Some(a1) = (*args).get(1) {
                    let c = try!(eval(a1.clone(), env.clone()));
                    match *c {
                        False | Nil => {
                            if args.len() >= 4 {
                                if let Some(a3) = (*args).get(3) {
                                    ast = a3.clone();
                                    continue 'tco;
                                } else {
                                    return Err(Syntax("Invalid if call".to_owned()));
                                }
                            } else {
                                return Ok(nil_type());
                            }
                        }
                        _ => {
                            if let Some(a2) = (*args).get(2) {
                                ast = a2.clone();
                                continue 'tco;
                            } else {
                                return Err(Syntax("Invalid if call".to_owned()));
                            }
                        }
                    }
                } else {
                    return Err(Syntax("Invalid if call".to_owned()));
                }
            }
            "fn*" => {
                if let Some(a1) = args.get(1) {
                    if let Some(a2) = args.get(2) {
                        return Ok(defun_type(eval, a2.clone(), env, a1.clone()));
                    } else {
                        return Err(Syntax("Invalid fn* definition".to_owned()));
                    }
                } else {
                    return Err(Syntax("Invalid fn* definition".to_owned()));
                }
            }
            "eval" => {
                if let Some(a1) = (*args).get(1) {
                    ast = try!(eval(a1.clone(), env.clone()));
                    env = env_root(&env);
                    continue 'tco;
                } else {
                    return Err(Syntax("Invalid eval call".to_owned()));
                }
            }
            "quote" => {
                if let Some(a1) = (*args).get(1) {
                    return Ok(a1.clone());
                } else {
                    return Err(Syntax("Invalid quote call".to_owned()));
                }
            }
            "quasiquote" => {
                if let Some(a1) = (*args).get(1) {
                    ast = quasiquote(a1.clone());
                    continue 'tco;
                } else {
                    return Err(Syntax("Invalid quasiquote call".to_owned()));
                }
            }
            "defmacro!" => {
                if let (Some(a1), Some(a2)) = ((*args).get(1), (*args).get(2)) {
                    let r = try!(eval(a2.clone(), env.clone()));
                    match *r {
                        Defun(ref mfd) => {
                            match **a1 {
                                Sym(_) => {
                                    let mut new_mfd = mfd.clone();
                                    new_mfd.is_macro = true;
                                    let mf = defun_d_type(new_mfd);
                                    env_set(&env.clone(), a1.clone(), mf.clone());
                                    return Ok(mf);
                                }
                                _ => return Err(Syntax("def! of non-symbol".to_owned())),
                            }
                        }
                        _ => return Err(Syntax("def! of non-symbol".to_owned())),
                    }
                } else {
                    return Err(Syntax("Unable to get defmacro! args".to_owned()));
                }
            }
            "macroexpand" => {
                if let Some(a1) = (*args).get(1) {
                    return macroexpand(a1.clone(), env.clone());
                } else {
                    return Err(Syntax("Invalid macroexpand call".to_owned()));
                }
            }
            "try*" => {
                if let Some(a1) = (*args).get(1) {
                    match eval(a1.clone(), env.clone()) {
                        Ok(res) => return Ok(res),
                        Err(err) => {
                            if args.len() < 3 {
                                return Err(err);
                            }
                            if let Some(a2) = (*args).get(2) {
                                let cat = match **a2 {
                                    List(ref cat) => cat,
                                    _ => return Err(Syntax("invalid catch* clause".to_owned())),
                                };
                                if cat.len() != 3 {
                                    return Err(Arity("wrong arity to catch* clause".to_owned()));
                                }

                                let exc = match err {
                                    Val(v) => v,
                                    _ => strn_type(err.description().to_owned()),
                                };

                                if let (Some(c1), Some(c2)) = ((*cat).get(1), (*cat).get(2)) {
                                    if let Sym(_) = **c1 {
                                        let bind_env = env_new(Some(env.clone()));
                                        env_set(&bind_env, c1.clone(), exc);
                                        return eval(c2.clone(), bind_env);
                                    } else {
                                        return Err(Syntax("invalid catch* binding".to_owned()));
                                    }

                                } else {
                                    return Err(Syntax("Unable to get args".to_owned()));
                                }
                            } else {
                                return Err(err);
                            }
                        }
                    }
                } else {
                    return Err(Syntax("Invalid try* call".to_owned()));
                }
            }
            "hist" => {
                if let Some(i) = (*args).get(1) {
                    if let Int(idx) = **i {
                        if let Some(line) = linenoise::history_line(idx as i32) {
                            if let Ok(ast) = read_str(&line) {
                                return eval(ast, env);
                            } else {
                                return Err(Syntax("Unable to generate AST".to_owned()));
                            }
                        } else {
                            return Err(Syntax("Unable to find requested history index".to_owned()));
                        }
                    } else {
                        return Err(Syntax("Invalid argument to hist".to_owned()));
                    }
                } else {
                    let mut idx = 0;

                    while let Some(l) = linenoise::history_line(idx) {
                        write!(io::stdout(), "{}: {}\n", idx, l)
                            .expect("Unable to write to stdout");
                        idx += 1;
                    }

                    return Ok(nil_type());
                }
            }
            _ => {
                // function call
                let el = try!(eval_ast(tmp.clone(), env.clone()));
                let form_args = match *el {
                    List(ref a) => a,
                    _ => return Err(Syntax("Invalid apply".to_owned())),
                };
                if let Some((f, fargs)) = form_args.split_first() {
                    return match **f {
                        Func(f) => f(fargs.to_vec()),
                        Defun(ref mf) => {
                            let mfc = mf.clone();
                            let arg_list = list_type(fargs.to_vec());
                            let new_env = env_new(Some(mfc.env.clone()));
                            match env_bind(&new_env, mfc.params, arg_list) {
                                Ok(_) => {
                                    ast = mfc.exp;
                                    env = new_env;
                                    continue 'tco;
                                }
                                Err(e) => Err(Syntax(e)),
                            }
                        }
                        _ => Err(Syntax("attempt to call non-function".to_owned())),
                    };
                } else {
                    return Err(Syntax("Invalid apply".to_owned()));
                }

            }
        }
    }
}
